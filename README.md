Implemented an SMS app which lets user send and receive sms. You can also see your previous inbox messages.

Libraries used
1)Appcompat support library
2)Design support library
3)Recyclerview
4)Cardview

compile 'com.android.support:appcompat-v7:23.1.1'
compile 'com.android.support:design:23.1.1'
compile 'com.android.support:recyclerview-v7:23.1.1'
compile 'com.android.support:cardview-v7:23.1.1'