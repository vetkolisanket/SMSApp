package com.example.sanket.smsapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.sanket.smsapp.R;
import com.example.sanket.smsapp.adapters.MessageAdapter;
import com.example.sanket.smsapp.models.SMS;

import java.util.List;

public class MessagesViewActivity extends AppCompatActivity {

    List<SMS> mSMSList;
    MessageAdapter mMessageAdapter;
    private String mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_view);

        Intent intent = getIntent();

        mTitle = intent.getStringExtra("senderName");
        mSMSList = intent.getParcelableArrayListExtra("messages");

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(mTitle);
            //actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setHomeButtonEnabled(true);
        }

        RecyclerView rvSMSMessages = (RecyclerView) findViewById(R.id.rv_sms_messages);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mMessageAdapter = new MessageAdapter(mSMSList);
        if (rvSMSMessages != null) {
            rvSMSMessages.setLayoutManager(linearLayoutManager);
            rvSMSMessages.setAdapter(mMessageAdapter);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint("Search by sender name or message");

        final ActionBar actionBar = getSupportActionBar();

        //set listeners
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mMessageAdapter.getFilter().filter(query.trim());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mMessageAdapter.getFilter().filter(newText.trim());
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actionBar != null) {
                    if (!searchView.isIconified()) {
                        actionBar.setTitle("");
                    } else {
                        actionBar.setTitle(mTitle);
                    }
                }
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if(actionBar != null) {
                    actionBar.setTitle(mTitle);
                }
                return false;
            }
        });
        return true;
    }
}
