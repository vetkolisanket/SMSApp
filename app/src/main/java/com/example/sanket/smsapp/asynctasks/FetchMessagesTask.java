package com.example.sanket.smsapp.asynctasks;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Telephony;

import com.example.sanket.smsapp.activities.MainActivity;
import com.example.sanket.smsapp.models.SMS;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by sanket on 4/24/2016.
 */
public class FetchMessagesTask extends AsyncTask<Void, Void, LinkedHashMap<String, List<SMS>>> {

    WeakReference<Context> mContextWeakReference;

    public FetchMessagesTask(Context context) {
        mContextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected LinkedHashMap<String, List<SMS>> doInBackground(Void... params) {
        Context context = mContextWeakReference.get();
        if (context != null) {
            LinkedHashMap<String, List<SMS>> smsMap = new LinkedHashMap<>();
            ContentResolver contentResolver = context.getContentResolver();
            Cursor cursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null);
            if (cursor != null) {
                int indexBody = cursor.getColumnIndex(Telephony.TextBasedSmsColumns.BODY);
                int indexAddress = cursor.getColumnIndex(Telephony.TextBasedSmsColumns.ADDRESS);
                int indexDateReceived = cursor.getColumnIndex(Telephony.TextBasedSmsColumns.DATE);
                if (indexBody < 0 || !cursor.moveToFirst()) return null;
                do {
                    SMS sms = new SMS();
                    sms.name = cursor.getString(indexAddress);
                    sms.message = cursor.getString(indexBody);
                    sms.time = cursor.getLong(indexDateReceived);
                    if (smsMap.containsKey(sms.name)) {
                        smsMap.get(sms.name).add(sms);
                    } else {
                        List<SMS> smsList = new ArrayList<>();
                        smsList.add(sms);
                        smsMap.put(sms.name, smsList);
                    }
                } while (cursor.moveToNext());

                cursor.close();
            }
            return smsMap;
        }
        return null;
    }

    @Override
    protected void onPostExecute(LinkedHashMap<String, List<SMS>> smsMap) {
        super.onPostExecute(smsMap);
        Context context = mContextWeakReference.get();
        if (context != null && smsMap != null) {
            ((MainActivity) context).updateMessageList(smsMap);
        }
    }
}
