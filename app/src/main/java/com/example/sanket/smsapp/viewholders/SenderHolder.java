package com.example.sanket.smsapp.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sanket.smsapp.R;
import com.example.sanket.smsapp.activities.MainActivity;

/**
 * Created by sanket on 4/23/2016.
 */
public class SenderHolder extends RecyclerView.ViewHolder {

    public TextView tvSenderName;
    public TextView tvNoOfMessages;
    public LinearLayout llSender;

    public SenderHolder(View itemView) {
        super(itemView);
        tvSenderName = (TextView) itemView.findViewById(R.id.tv_sender_name);
        tvNoOfMessages = (TextView) itemView.findViewById(R.id.tv_no_of_messages);
        llSender = (LinearLayout) itemView.findViewById(R.id.ll_sender);

        llSender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) v.getContext()).startMessagesViewActivity(tvSenderName.getText().toString());
            }
        });
    }
}
