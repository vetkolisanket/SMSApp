package com.example.sanket.smsapp.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.sanket.smsapp.R;

/**
 * Created by sanket on 4/23/2016.
 */
public class MessageHolder extends RecyclerView.ViewHolder {

    public TextView tvName;
    public TextView tvMessage;
    public TextView tvTime;

    public MessageHolder(View itemView) {
        super(itemView);

        tvName = (TextView) itemView.findViewById(R.id.tv_name);
        tvMessage = (TextView) itemView.findViewById(R.id.tv_message);
        tvTime = (TextView) itemView.findViewById(R.id.tv_time);
    }
}
