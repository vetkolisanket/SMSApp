package com.example.sanket.smsapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.sanket.smsapp.R;
import com.example.sanket.smsapp.models.SMSInfo;
import com.example.sanket.smsapp.viewholders.SenderHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by sanket on 4/23/2016.
 */
public class SenderAdapter extends RecyclerView.Adapter<SenderHolder> implements Filterable {

    List<SMSInfo> mSMSInfoList;
    List<SMSInfo> mFilteredSMSInfoList;

    SMSFilter mSMSFilter;

    public SenderAdapter(List<SMSInfo> smsInfos) {
        mSMSInfoList = smsInfos;
        mFilteredSMSInfoList = new ArrayList<>(smsInfos);
    }

    @Override
    public SenderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SenderHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_sender, parent, false));
    }

    @Override
    public void onBindViewHolder(SenderHolder holder, int position) {
        SMSInfo smsInfo = mFilteredSMSInfoList.get(position);
        holder.tvSenderName.setText(smsInfo.senderName);
        holder.tvNoOfMessages.setText(String.format(Locale.getDefault(), "%d messages", smsInfo.messageCount));
    }

    @Override
    public int getItemCount() {
        return mFilteredSMSInfoList.size();
    }

    @Override
    public Filter getFilter() {
        if(mSMSFilter == null) {
            mSMSFilter = new SMSFilter();
        }
        return mSMSFilter;
    }

    private class SMSFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            List<SMSInfo> filteredSMSInfoList = new ArrayList<>();

            if(constraint == null){
                filteredSMSInfoList.addAll(mSMSInfoList);
            } else {
                for (int i = 0, size = mSMSInfoList.size(); i < size; i++) {
                    SMSInfo smsInfo = mSMSInfoList.get(i);
                    if(smsInfo.senderName.toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filteredSMSInfoList.add(smsInfo);
                    }
                }
            }
            filterResults.values = filteredSMSInfoList;
            filterResults.count = filteredSMSInfoList.size();
            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilteredSMSInfoList = (ArrayList<SMSInfo>) results.values;
            notifyDataSetChanged();
        }
    }
}
