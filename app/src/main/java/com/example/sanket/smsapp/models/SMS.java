package com.example.sanket.smsapp.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sanket on 4/23/2016.
 */
public class SMS implements Parcelable {

    public String name;

    public String message;

    public long time;

    protected SMS(Parcel in) {
        name = in.readString();
        message = in.readString();
        time = in.readLong();
    }

    public SMS(){
    }

    public static final Creator<SMS> CREATOR = new Creator<SMS>() {
        @Override
        public SMS createFromParcel(Parcel in) {
            return new SMS(in);
        }

        @Override
        public SMS[] newArray(int size) {
            return new SMS[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(message);
        dest.writeLong(time);
    }
}
