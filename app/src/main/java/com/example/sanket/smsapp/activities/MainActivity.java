package com.example.sanket.smsapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.example.sanket.smsapp.R;
import com.example.sanket.smsapp.adapters.SenderAdapter;
import com.example.sanket.smsapp.asynctasks.FetchMessagesTask;
import com.example.sanket.smsapp.models.SMS;
import com.example.sanket.smsapp.models.SMSInfo;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    List<SMSInfo> mSMSInfo;
    SenderAdapter mSenderAdapter;
    Map<String, List<SMS>> mSMSMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSMSInfo = new ArrayList<>();
        mSMSMap = new LinkedHashMap<>();

        RecyclerView rvSMSMessages = (RecyclerView) findViewById(R.id.rv_sms_senders);
        mSenderAdapter = new SenderAdapter(mSMSInfo);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        if (rvSMSMessages != null) {
            rvSMSMessages.setLayoutManager(linearLayoutManager);
            rvSMSMessages.setAdapter(mSenderAdapter);
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();*/
                    startSMSComposeActivity();
                }
            });
        }

        new FetchMessagesTask(this).execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mNewMessageReceiver, new IntentFilter("newMessage"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNewMessageReceiver);
    }

    private void startSMSComposeActivity() {
        Intent intent = new Intent(this, SMSComposeActivity.class);
        startActivity(intent);
    }

    public void updateMessageList(LinkedHashMap<String, List<SMS>> smsMap) {
        mSMSInfo.clear();
        mSMSMap.clear();
        mSMSMap.putAll(smsMap);
        for (String senderName : mSMSMap.keySet()) {
            SMSInfo smsInfo = new SMSInfo();
            smsInfo.senderName = senderName;
            smsInfo.messageCount = mSMSMap.get(senderName).size();
            mSMSInfo.add(smsInfo);
        }
        mSenderAdapter.getFilter().filter("");
    }

    public void startMessagesViewActivity(String senderName) {
        Intent intent = new Intent(this, MessagesViewActivity.class);
        intent.putExtra("senderName", senderName);
        intent.putParcelableArrayListExtra("messages", (ArrayList<SMS>) mSMSMap.get(senderName));
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setIconifiedByDefault(true);
        searchView.setQueryHint("Search by sender name");

        final ActionBar actionBar = getSupportActionBar();

        //set listeners
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mSenderAdapter.getFilter().filter(query.trim());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mSenderAdapter.getFilter().filter(newText.trim());
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actionBar != null) {
                    if (!searchView.isIconified()) {
                        actionBar.setTitle("");
                    } else {
                        actionBar.setTitle(getString(R.string.app_name));
                    }
                }
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if (actionBar != null) {
                    actionBar.setTitle(getString(R.string.app_name));
                }
                return false;
            }
        });
        return true;
    }


    private BroadcastReceiver mNewMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            new FetchMessagesTask(MainActivity.this).execute();
        }
    };
}
