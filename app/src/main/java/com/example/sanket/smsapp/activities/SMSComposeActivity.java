package com.example.sanket.smsapp.activities;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sanket.smsapp.R;

public class SMSComposeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smscompose);

        final TextInputLayout tilPhoneNo = (TextInputLayout) findViewById(R.id.til_phone_no);
        final TextInputLayout tilMessage = (TextInputLayout) findViewById(R.id.til_message);
        final EditText etPhoneNo = (EditText) findViewById(R.id.et_phone_no);
        final EditText etMessage = (EditText) findViewById(R.id.et_message);
        Button btnSend = (Button) findViewById(R.id.btn_send);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            //actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setHomeButtonEnabled(true);
        }

        if (btnSend != null) {
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (etPhoneNo != null) {
                        String phoneNo = etPhoneNo.getText().toString();
                        if(isValidNo(phoneNo)) {
                            if (tilPhoneNo != null) {
                                tilPhoneNo.setErrorEnabled(false);
                            }
                            if (etMessage != null) {
                                String message = etMessage.getText().toString();
                                if(!TextUtils.isEmpty(message)) {
                                    if (tilMessage != null) {
                                        tilMessage.setErrorEnabled(false);
                                    }
                                    sendSMS(phoneNo, message);
                                } else {
                                    if (tilMessage != null) {
                                        tilMessage.setError("Message field is empty!");
                                    }
                                }
                            }
                        } else {
                            if (tilPhoneNo != null) {
                                tilPhoneNo.setError("Enter a valid phone no!");
                            }
                        }
                    }
                }
            });
        }
    }

    private void sendSMS(String phoneNo, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        try {
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(SMSComposeActivity.this, "SMS sent successfully!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(SMSComposeActivity.this, "SMS sending failed", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private boolean isValidNo(String phoneNo) {
        if(TextUtils.isEmpty(phoneNo)){
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
