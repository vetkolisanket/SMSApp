package com.example.sanket.smsapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.sanket.smsapp.R;
import com.example.sanket.smsapp.common.Utils;
import com.example.sanket.smsapp.models.SMS;
import com.example.sanket.smsapp.viewholders.MessageHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanket on 4/23/2016.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageHolder> implements Filterable {

    List<SMS> mSMSList;
    List<SMS> mFilteredSMSList;

    SMSMessageFilter mSMSMessageFilter;

    public MessageAdapter(List<SMS> smsList) {
        mSMSList = smsList;
        mFilteredSMSList = new ArrayList<>(smsList);
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MessageHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_message, parent, false));
    }

    @Override
    public void onBindViewHolder(MessageHolder holder, int position) {
        SMS sms = mFilteredSMSList.get(position);
        holder.tvName.setText(sms.name);
        holder.tvMessage.setText(sms.message);
        holder.tvTime.setText(Utils.formatToDate(sms.time));
    }

    @Override
    public int getItemCount() {
        return mFilteredSMSList.size();
    }

    @Override
    public Filter getFilter() {
        if(mSMSMessageFilter == null) {
            mSMSMessageFilter = new SMSMessageFilter();
        }
        return mSMSMessageFilter;
    }

    private class SMSMessageFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            List<SMS> filteredSMSList = new ArrayList<>();

            for (int i = 0, size = mSMSList.size(); i < size; i++) {
                SMS sms = mSMSList.get(i);
                if(sms.name.toLowerCase().contains(constraint.toString().toLowerCase())
                        || sms.message.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    filteredSMSList.add(sms);
                }
            }
            filterResults.values = filteredSMSList;
            filterResults.count = filteredSMSList.size();
            return filterResults;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilteredSMSList = (ArrayList<SMS>) results.values;
            notifyDataSetChanged();
        }
    }
}
