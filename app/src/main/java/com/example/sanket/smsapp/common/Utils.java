package com.example.sanket.smsapp.common;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sanket on 4/23/2016.
 */
public class Utils {

    private static final String DATE_FORMAT = "MMM d, yyyy";
    private static final String TIME_FORMAT = "hh:mma";
    private static final String MESSAGE_TIME_FORMAT = "dd MMM yy, hh:mma";

    public static String formatToDate(long timeStamp) {
        Date date = new Date(timeStamp);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR)
                && calendar.get(Calendar.DAY_OF_YEAR) == today
                .get(Calendar.DAY_OF_YEAR)) {
            return "Today, " + getTime(timeStamp, TIME_FORMAT);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR)
                && calendar.get(Calendar.DAY_OF_YEAR) == yesterday
                .get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday, " + getTime(timeStamp, TIME_FORMAT);
        } else {
            return Utils.getDate(timeStamp, MESSAGE_TIME_FORMAT);
        }
    }

    public static String getTime(long timestamp, String format) {
        Date time = new Date(timestamp);
        SimpleDateFormat timeFormatter = new SimpleDateFormat(format, Locale.getDefault());
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[]{"am", "pm"});
        timeFormatter.setDateFormatSymbols(symbols);
        return timeFormatter.format(time);
    }

    public static String getDate(long timestamp, String format) {
        Date date = new Date(timestamp);
        SimpleDateFormat dateFormatter = new SimpleDateFormat(format, Locale.getDefault());
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[]{"am", "pm"});
        dateFormatter.setDateFormatSymbols(symbols);
        return dateFormatter.format(date);
    }

}
